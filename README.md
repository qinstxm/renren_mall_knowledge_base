# 人人商城知识库

#### 介绍
人人商城知识库。

#### 软件架构
软件架构说明


#### 安装教程
- 源码运行环境：
亲，源码最佳运行环境：PHP5.3/5.4/5.5/5.6+MYSQL5.1/5.5/5.6。必须开启openssl、redis（秒杀需要用到）扩展，源码需要安装在根目录，源码约150M
此源码必须安装在一级目录，不支持二级目录
使用条件：备案域名（不支持IP）+认证服务号

- 安装步骤：
1、把源码上传到网站根目录
2、创建一个数据库,开用户
然后导入数据库文件（源码根目录下的wesambofx.sql）到数据库
3、修改数据库
打开：根目录data/config.php
配置服务器
$config['db']['master']['host'] = 'localhost';
$config['db']['master']['username'] = 'wesambofx';-->你的数据库用户名
$config['db']['master']['password'] = 'wesambofx';-->你的数据库密码
$config['db']['master']['port'] = '3306';         -->你的数据库端口（默认3306）
$config['db']['master']['database'] = 'wesambofx';-->你的数据库名称
slave部分不需要配置
- 安装完成
管理地址：http://域名
默认账号密码都是admin  admin123
如果出现空白或者500服务器内部错误可按照以下方法调试
把根目录/data/config.php里面的development改成1，就可以看到详细错误，调试完之后，记得把development改回0

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

#### 人人商城产品上传及页面装修

- [进入商城](https://su.mhcaizhuan.com)账号密码请联系管理获取

> 平台入口-->人人商城V3-->应用-->页面管理-->系统页面
![输入图片说明](https://images.gitee.com/uploads/images/2019/1209/143648_62cb7e67_5324149.png "微信图片_20191209142956.png")

- 进入管理 
> 新建页面-->立即创建-->商城首页

![输入图片说明](https://images.gitee.com/uploads/images/2019/1209/143713_d7577521_5324149.png "QQ截图20191209143256.png")

- 页面展示
![输入图片说明](https://images.gitee.com/uploads/images/2019/1209/144327_f9a50f8c_5324149.jpeg "未标题-1.jpg")
- 版面功能（根据需要制作）

![输入图片说明](https://images.gitee.com/uploads/images/2019/1209/145815_a9fb94c4_5324149.png "QQ截图20191209145242.png")
